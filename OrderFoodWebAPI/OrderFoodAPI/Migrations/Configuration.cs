﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using OrderFoodAPI.DAL;
using OrderFoodAPI.Models.Entities;

namespace OrderFoodAPI.Migrations
{

    internal sealed class Configuration : DbMigrationsConfiguration<OrderFoodDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(OrderFoodDBContext context)
        {

        }
    }
}
