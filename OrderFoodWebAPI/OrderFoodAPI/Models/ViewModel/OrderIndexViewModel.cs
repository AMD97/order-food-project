﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderFoodAPI.Models.ViewModel
{
    public class OrderIndexViewModel
    {

        public int ID { get; set; }
        public string Date { get; set; }
        public double Total { get; set; }

    }
}