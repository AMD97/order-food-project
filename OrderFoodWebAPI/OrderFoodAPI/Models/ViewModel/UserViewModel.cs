﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderFoodAPI.Models.ViewModel
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string RegistrationDate { get; set; }

    }
}