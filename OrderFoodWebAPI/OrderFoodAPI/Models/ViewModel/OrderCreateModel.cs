﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OrderFoodAPI.Models.ViewModel
{
    public class OrderCreateModel
    {
        
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }

        [Required]
        public bool NewInformation { get; set; }

        [Required]
        public ICollection<DetailCreateModel> OrderDetails { get; set; }

        public class DetailCreateModel
        {
            [Required]
            public int ID { get; set; }

            [Required]
            public int Quantity { get; set; }
        }

    }
}