﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderFoodAPI.Models.ViewModel
{
    public class QueryDateModel
    {
        public DateTime start { get; set; }
        public DateTime end { get; set; }

    }
}