﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OrderFoodAPI.Models.Entities
{
    public abstract class EntityBase
    {
        [Key]
        public int ID { get; set; }
    }
}