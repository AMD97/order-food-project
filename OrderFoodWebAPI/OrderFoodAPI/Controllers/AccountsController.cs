﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using Microsoft.AspNet.Identity;
using OrderFoodAPI.AutoMapper;
using OrderFoodAPI.Models.Entities;
using OrderFoodAPI.Models.ViewModel;

namespace OrderFoodAPI.Controllers
{
    [RoutePrefix("api/accounts")]
    public class AccountsController : BaseIdentityController
    {

        [Authorize(Roles = "Admin")]
        [HttpGet]
        [Route("users")]
        public async IHttpActionResult GetUsers()
        {
            var users = await AppUserManager.Users.ToListAsync();
            var usersView = Mapper.Map<IEnumerable<UserViewModel>>(users);

            return Ok(usersView);
        }

        [Authorize]
        [HttpGet]
        [Route("user")]
        public async IHttpActionResult GetUser()
        {
            var user = await AppUserManager.FindByIdAsync(User.Identity.GetUserId<int>());
            if (user == null)
                return NotFound();

            var userView = Mapper.Map<UserViewModel>(user);

            return Ok(userView);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("create")]
        public async Task<IHttpActionResult> CreateUser([FromBody] UserCreateModel userCreateModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = Mapper.Map<User>(userCreateModel);
            user.EmailConfirmed = true;
            IdentityResult addUserResult = await AppUserManager.CreateAsync(user, userCreateModel.Password);

            if (!addUserResult.Succeeded)
                return GetErrorResult(addUserResult);

             await AppUserManager.AddToRoleAsync(user.Id, "Customer");

            var userView = Mapper.Map<UserViewModel>(user);

            return Ok(userView);
        }

        [Authorize]
        [Route("ChangePassword")]
        [HttpPost]
        public async Task<IHttpActionResult> ChangePassword([FromBody] ChangePasswordViewModel passModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            
            IdentityResult result = await AppUserManager.ChangePasswordAsync(User.Identity.GetUserId<int>(), passModel.OldPassword, passModel.NewPassword);

            if (!result.Succeeded)
                return GetErrorResult(result);

            return Ok();
        }

        [Authorize]
        [Route("Update")]
        [HttpPost]
        public async Task<IHttpActionResult> UpdateUser([FromBody] UpdateUserModel updateModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = await AppUserManager.FindByIdAsync(User.Identity.GetUserId<int>());

            user.FirstName = updateModel.FirstName;
            user.LastName = updateModel.LastName;
            user.Address = updateModel.Address;
            user.PhoneNumber = updateModel.PhoneNumber;

            IdentityResult result = await AppUserManager.UpdateAsync(user);

            if (!result.Succeeded)
                return GetErrorResult(result);

            return Ok();
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        [Route("DeleteUser/{id}")]
        public async Task<IHttpActionResult> DeleteUser(int id)
        {

            var appUser = await AppUserManager.FindByIdAsync(id);

            if (appUser != null)
            {
                IdentityResult result = await AppUserManager.DeleteAsync(appUser);

                if (!result.Succeeded)
                    return GetErrorResult(result);

                return Ok();
            }
            return NotFound();
        }
    }
}
