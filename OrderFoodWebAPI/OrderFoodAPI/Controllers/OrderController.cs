﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.UI;
using AutoMapper;
using Microsoft.AspNet.Identity;
using OrderFoodAPI.DAL.UnitOfWork;
using OrderFoodAPI.Models.Entities;
using OrderFoodAPI.Models.ViewModel;

namespace OrderFoodAPI.Controllers
{
    [RoutePrefix("api")]
    public class OrderController : BaseIdentityController
    {
        private readonly IUnitOfWork _context;

        public OrderController()
        {
            _context = new UnitOfWork();
        }

        [Authorize(Roles = "Admin")]
        [Route("Orders/page/{page}")]
        [HttpGet]
        public IHttpActionResult IndexOrder(int page, int pageSize = 5)
        {
            try
            {
                var order = _context.Order.GetPagingOrder(page, pageSize);
                var orderIndexView = Mapper.Map<IEnumerable<OrderIndexViewModel>>(order);
                return Ok(orderIndexView);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }

        }

        [Route("Orders/user")]
        [Authorize]
        [HttpGet]
        public IHttpActionResult GetOrderByUserID()
        {
            try
            {
                var userId = User.Identity.GetUserId<int>();

                var order = _context.Order.GetOrderByUserID(userId);
                if (order == null)
                    return NotFound();

                var orderIndexView = Mapper.Map<IEnumerable<OrderIndexViewModel>>(order);
                return Ok(orderIndexView);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [Authorize]
        [Route("Order/{id}")]
        [HttpGet]
        public IHttpActionResult GetOrderByID(int id)
        {
            try
            {
                var order = _context.Order.Get(id);
                if (order == null)
                    return NotFound(); 

                var orderView = Mapper.Map<OrderViewModel>(order);
                return Ok(orderView);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [Authorize]
        [Route("OrderDetails/{id}")]
        [HttpGet]
        public IHttpActionResult GetDetailByOrderID(int id)
        {
            try
            {
                var orderDetailView = _context.OrderDetail.GetOrderDetailViewByOrderId(id);
                return Ok(orderDetailView);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [Route("Order")]
        [HttpPost]
        [Authorize]
        public IHttpActionResult PostOrder([FromBody]OrderCreateModel orderCreateModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var orderDetails = new List<OrderDetail>();

                foreach (var detail in orderCreateModel.OrderDetails)
                {       
                    orderDetails.Add(new OrderDetail()
                    {
                        Product = _context.Product.Get(detail.ID),
                        Price = _context.Product.Get(detail.ID).Price,
                        Quantity = detail.Quantity,
                    });
                }

                var userId = User.Identity.GetUserId<int>();

                Order order;
                if (orderCreateModel.NewInformation)
                {
                    order = Mapper.Map<Order>(orderCreateModel);
                    order.UserID = userId;
                }
                else
                {
                    var user = await AppUserManager.FindByIdAsync(userId);
                    order = Mapper.Map<Order>(user);
                }

                order.Total = orderDetails.Sum(src => src.SumPrice());
                order.OrderDetails = orderDetails;
                order.Date = DateTime.Now;
                _context.Order.Insert(order);
                _context.Save();           

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

    }
}
