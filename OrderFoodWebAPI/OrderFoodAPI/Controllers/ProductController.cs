﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using OrderFoodAPI.DAL.UnitOfWork;
using OrderFoodAPI.Models.Entities;
using OrderFoodAPI.Models.ViewModel;

namespace OrderFoodAPI.Controllers
{
    [RoutePrefix("api")]

    public class ProductController : ApiController
    {
        private IUnitOfWork _context;

        public ProductController()
        {
            _context = new UnitOfWork();  
        }

        [Route("Products/page/{page}")]
        [HttpGet]
        public IHttpActionResult IndexProduct(int page, int pageSize = 5)
        {
            try
            {
                var product = _context.Product.GetPaging(page, pageSize);
          
                if (product == null)
                    return NotFound();

                var productIndexView = Mapper.Map<IEnumerable<ProductIndexViewModel>>(product);
                return Ok(productIndexView);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [Route("Product/{id}")]
        [HttpGet]
        public IHttpActionResult GetProductByID(int id)
        {
            try
            {
                var product = _context.Product.Get(id);
                if (product == null)
                    return NotFound();
 
                var productView = Mapper.Map<ProductViewModel>(product);
                return Ok(productView);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [Route("Product")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IHttpActionResult PostProduct([FromBody]ProductViewModel productView)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var product = Mapper.Map<Product>(productView);


                _context.Product.Insert(product);
                _context.Save();
               
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [Route("Product/{id}")]
        [Authorize(Roles = "Admin")]
        [HttpPut]
        public IHttpActionResult PutProduct(int id, [FromBody]ProductViewModel productView)
        {
            try
            {
                if (!(ModelState.IsValid) || productView.ID != id)
                    return BadRequest(ModelState);

                var found = _context.Product.Exist(id);
                if (!found)
                    return NotFound();

                var product = Mapper.Map<Product>(productView);

                _context.Product.Update(product);
                _context.Save();         
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [Route("Product/{id}")]
        [Authorize(Roles = "Admin")]
        [HttpDelete]
        public IHttpActionResult DeleteProduct(int id)
        {
            try
            {
                var product = _context.Product.Get(id);
                if (product == null)
                    return NotFound();

                _context.Product.Delete(id);
                _context.Save();
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [Authorize]
        [Route("Product/MostSales")]
        [HttpGet]
        public IHttpActionResult getMostSales([FromBody] QueryDateModel model)
        {

            DateTime start = model.start;
            DateTime end = model.end;

            IEnumerable<Product> products = _context.Product.getTopSales(start, end);

            var productIndexView = Mapper.Map<IEnumerable<ProductIndexViewModel>>(products);
            return Ok(productIndexView);

        }

        [Route("Categories")]
        [Authorize]
        [HttpGet]
        public IHttpActionResult GetCategory()
        {
            try
            {
                var category = _context.Category.Get().ToList();
                var categoryView = Mapper.Map<IEnumerable<CategoryViewModel>>(category);
                return Ok(categoryView);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [Route("Category/{id}")]
        [Authorize]
        [HttpGet]
        public IHttpActionResult GetCategoryByID(int id)
        {
            try
            {
                var category = _context.Category.Get(id);
                if (category == null)
                    return NotFound();

                var categoryView = Mapper.Map<CategoryViewModel>(category);
                return Ok(categoryView);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [Route("Category")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IHttpActionResult PostCategory([FromBody]Category category)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                _context.Category.Insert(category);
                _context.Save();

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [Route("Category")]
        [Authorize(Roles = "Admin")]
        [HttpPut]
        public IHttpActionResult PutCategory(int id, [FromBody]Category category)
        {
            try
            {
                if (!(ModelState.IsValid) || category.ID != id)
                    return BadRequest(ModelState);

                var found = _context.Category.Exist(id);
                if (!found)
                    return NotFound();

                _context.Category.Update(category);
                _context.Save();
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [Route("Category/{id}")]
        [Authorize(Roles = "Admin")]
        [HttpDelete]
        public IHttpActionResult DeleteCategory(int id)
        {
            try
            {
                var category = _context.Category.Get(id);
                if (category == null)
                    return NotFound();

                _context.Category.Delete(id);
                _context.Save();
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

    }

}
