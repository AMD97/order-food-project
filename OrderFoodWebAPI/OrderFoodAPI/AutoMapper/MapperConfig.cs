﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using AutoMapper;
using OrderFoodAPI.Models;
using OrderFoodAPI.Models.Entities;
using OrderFoodAPI.Models.ViewModel;

namespace OrderFoodAPI.AutoMapper
{
    public class MapperConfig : Profile
    {
        protected override void Configure()
        {
            CreateMap<DateTime, string>().ConvertUsing(new DateTimeToShamsiConverter());

            CreateMap<ProductViewModel, Product>();

            CreateMap<Product, ProductViewModel>()
                .ForMember(des => des.CategoryName, opt => opt.MapFrom(src => src.Category.Name));

            CreateMap<Product, ProductIndexViewModel>()
                .ForMember(des => des.CategoryName, opt => opt.MapFrom(src => src.Category.Name));

            CreateMap<Category, CategoryViewModel>().ReverseMap();

            CreateMap<User, UserViewModel>()
                .ForMember(des => des.Username,opt =>opt.MapFrom(src => src.UserName));
            CreateMap<UserCreateModel, User>()
                .ForMember(des => des.RegistrationDate,
                opt => opt.UseValue(DateTime.Now.ToLocalTime()));

            CreateMap<Order, OrderViewModel>();
            CreateMap<Order, OrderIndexViewModel>();
            CreateMap<OrderDetail, OrderDetailViewModel>();

            CreateMap<User, Order>()
                .ForMember(des => des.UserID, opt => opt.MapFrom(src => src.Id))
                .ForMember(des => des.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(des => des.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(des => des.Address, opt => opt.MapFrom(src => src.Address))
                .ForMember(des => des.Phone, opt => opt.MapFrom(src => src.PhoneNumber))
                .ForMember(des => des.Date, opt => opt.UseValue(DateTime.Now));

            CreateMap<OrderCreateModel, Order>()
                .ForMember(des => des.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(des => des.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(des => des.Address, opt => opt.MapFrom(src => src.Address))
                .ForMember(des => des.Phone, opt => opt.MapFrom(src => src.Phone))
                .ForMember(des => des.Date, opt => opt.UseValue(DateTime.Now));

        }

    }
}