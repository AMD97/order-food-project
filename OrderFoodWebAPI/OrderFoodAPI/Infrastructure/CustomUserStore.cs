﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;
using OrderFoodAPI.DAL;
using OrderFoodAPI.Models.Entities;

namespace OrderFoodAPI.Infrastructure
{
    public class CustomUserStore : UserStore<User, Role, int,
        UserLogin, UserRole, UserClaim>
    {
        public CustomUserStore(OrderFoodDBContext context) : base(context)
        {
        }
    }
}