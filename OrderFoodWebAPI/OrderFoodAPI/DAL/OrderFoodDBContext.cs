﻿using System;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using OrderFoodAPI.Models.Entities;

namespace OrderFoodAPI.DAL
{
    public class OrderFoodDBContext : IdentityDbContext<User, Role, int, UserLogin, UserRole, UserClaim>
    {
        public OrderFoodDBContext() : base("OrderFood")
        {
//            Configuration.LazyLoadingEnabled = false;
//            Configuration.ProxyCreationEnabled = false;
        }
        public static OrderFoodDBContext Create()
        {
            return new OrderFoodDBContext();
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<OrderDetail> OrderDetail { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<Product> Product { get; set; }
    }
}