﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderFoodAPI.DAL.Repositories;
using OrderFoodAPI.Models;
using OrderFoodAPI.Models.Entities;

namespace OrderFoodAPI.DAL.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {

        ICategoryRepository Category { get; }
        IOrderRepository Order { get; }
        IOrderDetailRepository OrderDetail { get; }
        IProductRepository Product { get; }
        void Save();

    }
}
