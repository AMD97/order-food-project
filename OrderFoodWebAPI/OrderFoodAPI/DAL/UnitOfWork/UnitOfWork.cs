﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Web;
using OrderFoodAPI.DAL.Repositories;
using OrderFoodAPI.Models;
using OrderFoodAPI.Models.Entities;

namespace OrderFoodAPI.DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private OrderFoodDBContext _context;
        private ICategoryRepository _category;
        private IOrderRepository _order;
        private IOrderDetailRepository _orderDetail;
        private IProductRepository _product;

        public UnitOfWork()
        {
            _context = new OrderFoodDBContext();
        }

        public ICategoryRepository Category
        {
            get
            {
                if (_category == null)
                    _category = new CategoryRepository(_context);
                return _category;
            }
        }

        public IOrderRepository Order
        {
            get
            {
                if (_order == null)
                    _order = new OrderRepository(_context);
                return _order;
            }
        }

        public IOrderDetailRepository OrderDetail
        {
            get
            {
                if (_orderDetail == null)
                    _orderDetail = new OrderDetailRepository(_context);
                return _orderDetail;
            }
        }

        public IProductRepository Product
        {
            get
            {
                if (_product == null)
                    _product = new ProductRepository(_context);
                return _product;
            }
        }

        public void Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                throw e;
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}