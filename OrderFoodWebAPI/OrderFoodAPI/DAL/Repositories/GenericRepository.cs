﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Ninject.Infrastructure.Language;
using OrderFoodAPI.Models;
using OrderFoodAPI.Models.Entities;

namespace OrderFoodAPI.DAL.Repositories
{
    public abstract class GenericRepository<T> : IGenericRepository<T> where T : EntityBase  
    {

        private OrderFoodDBContext context;
        private DbSet<T> DbSet;

        public GenericRepository(OrderFoodDBContext context)
        {
            this.context = context;
            DbSet = context.Set<T>();
        }

        public virtual IEnumerable<T> Get()
        {
            return DbSet.AsQueryable();
        }

        public virtual T Get(int id)
        {
            return DbSet.Find(id);
        }

        public virtual T Update(T obj)
        {
            context.Entry(obj).State = EntityState.Modified;
            return obj;
        }

        public virtual T Insert(T obj)
        {
            DbSet.Add(obj);
            return obj;
        }

        public virtual void Delete(int id)
        {
            T entityToDelete = DbSet.Find(id);
            DbSet.Remove(entityToDelete);
        }

        public virtual int Delete(T obj)
        {
            if (context.Entry(obj).State == EntityState.Detached)
            {
                DbSet.Attach(obj);
            }
            DbSet.Remove(obj);

            return context.SaveChanges();
        }

        public  bool Exist(int id)
        {
            return DbSet.Any(p => p.ID == id);
        }

        public IEnumerable<T> GetPaging(int page, int pageSize)
        {
            page--;

            IEnumerable<T> result = DbSet
                .OrderBy(c => c.ID)
                .Skip(pageSize*page)
                .Take(pageSize)
                .ToEnumerable();

            return result;
        }
    }
}