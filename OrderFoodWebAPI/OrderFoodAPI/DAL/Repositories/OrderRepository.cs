﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject.Infrastructure.Language;
using OrderFoodAPI.Models;
using OrderFoodAPI.Models.Entities;
using DbSet = System.Data.Entity.DbSet;

namespace OrderFoodAPI.DAL.Repositories
{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        private OrderFoodDBContext _context;

        public OrderRepository(OrderFoodDBContext context) : base(context)
        {
            _context = context;

        }

        public void UpdateTotal(Order obj)
        {
            _context.Order.Attach(obj);
            _context.Entry(obj).Property(u => u.Total).IsModified = true;
            _context.SaveChanges();
        }

        public IEnumerable<Order> GetPagingOrder(int page, int pageSize)
        {
            page--;

            IEnumerable<Order> result = _context.Order
                .OrderByDescending(c => c.Date)
                .Skip(pageSize * page)
                .Take(pageSize)
                .ToEnumerable();

            return result;
        }

        public IEnumerable<Order> GetOrderByUserID(int id)
        {
            var query =
                from order in _context.Order
                where order.UserID == id
                orderby order.Date descending 
                select order;

            return query.ToList();
        }
    }
}