﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderFoodAPI.Models.Entities;

namespace OrderFoodAPI.DAL.Repositories
{
    public interface IGenericRepository<T> where T : EntityBase 
    {

        IEnumerable<T> Get();

        T Get(int id);

        T Update(T obj);

        T Insert(T obj);

        int Delete(T obj);

        void Delete(int id);

        bool Exist(int id);

        IEnumerable<T> GetPaging(int page, int pageSize);
    }
}
