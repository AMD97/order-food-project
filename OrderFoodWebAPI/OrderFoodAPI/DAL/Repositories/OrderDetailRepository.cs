﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject.Infrastructure.Language;
using OrderFoodAPI.Models;
using OrderFoodAPI.Models.Entities;
using OrderFoodAPI.Models.ViewModel;

namespace OrderFoodAPI.DAL.Repositories
{
    public class OrderDetailRepository : GenericRepository<OrderDetail>, IOrderDetailRepository
    {
        private OrderFoodDBContext _context;

        public OrderDetailRepository(OrderFoodDBContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<OrderDetailViewModel> GetOrderDetailViewByOrderId(int id)
        {
            var query =
                from detail in _context.OrderDetail
                where detail.OrderID == id
                orderby detail.ID
                select new OrderDetailViewModel()
                {
                    ID = detail.ID,
                    OrderID = detail.OrderID,
                    Quantity = detail.Quantity,
                    ProductID = detail.ProductID,
                    ProductName = detail.Product.Name,
                    ProductPrice = detail.Price
                };

            return query;
        }

    }
}