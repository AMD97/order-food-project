﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject.Infrastructure.Language;
using OrderFoodAPI.Models;
using OrderFoodAPI.Models.Entities;
using OrderFoodAPI.Models.ViewModel;

namespace OrderFoodAPI.DAL.Repositories
{
    public class ProductRepository : GenericRepository<Product> ,IProductRepository
    {
        private OrderFoodDBContext _context;
        public ProductRepository(OrderFoodDBContext context) : base(context)
        {
            _context = context;
        }


        public IEnumerable<Product> getTopSales(DateTime start, DateTime end)
        {
            var query =
                (from p in _context.Product
                    let totalQuantity = (from detail in _context.OrderDetail
                        join order in _context.Order on detail.OrderID equals order.ID
                        where detail.ProductID == p.ID && order.Date >= start && order.Date <= end
                        select detail.Quantity).Sum()
                    where totalQuantity > 0
                    orderby totalQuantity descending
                    select p).Take(10);
            return query.ToList();
        }


    }
}