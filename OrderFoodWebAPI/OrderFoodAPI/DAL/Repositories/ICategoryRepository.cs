﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrderFoodAPI.Models.Entities;

namespace OrderFoodAPI.DAL.Repositories
{
    public interface ICategoryRepository : IGenericRepository<Category>
    {


    }
}