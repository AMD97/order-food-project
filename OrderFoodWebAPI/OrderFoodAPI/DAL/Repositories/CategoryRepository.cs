﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrderFoodAPI.Models;
using OrderFoodAPI.Models.Entities;

namespace OrderFoodAPI.DAL.Repositories
{
    public class CategoryRepository : GenericRepository<Category>, ICategoryRepository
    {
        private OrderFoodDBContext _context;

        public CategoryRepository(OrderFoodDBContext context) : base(context)
        {
            _context = context;

        }

    }
}