﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderFoodAPI.Models.Entities;

namespace OrderFoodAPI.DAL.Repositories
{
    public interface IOrderRepository : IGenericRepository<Order>
    {

        void UpdateTotal(Order obj);
        IEnumerable<Order> GetOrderByUserID(int id);
        IEnumerable<Order> GetPagingOrder(int page, int pageSize);

    }

}
