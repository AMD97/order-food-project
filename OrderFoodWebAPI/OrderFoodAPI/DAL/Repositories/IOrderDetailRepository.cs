﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderFoodAPI.Models.Entities;
using OrderFoodAPI.Models.ViewModel;

namespace OrderFoodAPI.DAL.Repositories
{
    public interface IOrderDetailRepository : IGenericRepository<OrderDetail>
    {

        IEnumerable<OrderDetailViewModel> GetOrderDetailViewByOrderId(int id);

    }
}
