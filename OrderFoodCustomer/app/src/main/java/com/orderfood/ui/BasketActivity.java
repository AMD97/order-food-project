package com.orderfood.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.orderfood.R;
import com.orderfood.dal.OrderDetailDB;
import com.orderfood.dal.entities.Order;
import com.orderfood.dal.entities.OrderDetail;
import com.orderfood.model.OrderCreateModel;
import com.orderfood.model.OrderDetailCreateModel;
import com.orderfood.model.UserModel;
import com.orderfood.restAPI.AccountAPI;
import com.orderfood.restAPI.OrderAPI;
import com.orderfood.restAPI.RestClient;
import com.orderfood.ui.adapter.DetailAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AMD on 5/31/2016.
 */
public class BasketActivity extends AppCompatActivity {

    OrderDetailDB dbContext;

    private EditText phoneNumberEditText;
    private EditText addressEditText;
    private EditText lastNameEditText;
    private EditText firstNameEditText;

    private CheckBox newInformationChkbox;

    private UserModel user;
    private Order order;
    private List<OrderDetail> orderDetails;
    private RecyclerView recyclerView;
    private DetailAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_basket);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        firstNameEditText = (EditText) findViewById(R.id.first_name);
        lastNameEditText = (EditText) findViewById(R.id.last_name);
        phoneNumberEditText = (EditText) findViewById(R.id.phone_number);
        addressEditText = (EditText) findViewById(R.id.address);
        newInformationChkbox = (CheckBox) findViewById(R.id.information_chkbox);

        recyclerView = (RecyclerView) findViewById(R.id.basket_RecyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        getUserAPI();
        getDetail();
    }

    private void getDetail() {

        dbContext = new OrderDetailDB(this);

        orderDetails = dbContext.findAll();

        adapter = new DetailAdapter(orderDetails);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }

    private void getUserAPI() {

        AccountAPI api = RestClient.createService(AccountAPI.class, this);
        Call<UserModel> call = api.getUser();
        call.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                if (response.isSuccessful()) {
                    user = response.body();
                    onUserSuccess();
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable throwable) {
                Toast.makeText(BasketActivity.this, R.string.connection_error, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void onUserSuccess() {

        firstNameEditText.setText(user.getFirstName());
        lastNameEditText.setText(user.getLastName());
        phoneNumberEditText.setText(user.getPhoneNumber());
        addressEditText.setText(user.getAddress());

    }


    private void postOrder() {

        OrderCreateModel model = new OrderCreateModel();

        if (newInformationChkbox.isChecked()) {
            model.setNewInformation(true);
            model.setFirstName(firstNameEditText.getText().toString());
            model.setLastName(lastNameEditText.getText().toString());
            model.setAddress(addressEditText.getText().toString());
            model.setPhone(phoneNumberEditText.getText().toString());
        } else {
            model.setNewInformation(false);
        }

        List<OrderDetailCreateModel> DetailCreateModel = new ArrayList<>();
        for (OrderDetail detail : orderDetails) {
            DetailCreateModel.add(new OrderDetailCreateModel(
                    detail.getId(),
                    detail.getQuantity()
            ));
        }

        model.setOrderDetails(DetailCreateModel);

        addOrderAPI(model);
    }

    private void addOrderAPI(OrderCreateModel model) {

        OrderAPI api = RestClient.createService(OrderAPI.class, this);
        Call<Void> call = api.addOrder(model);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    onSuccess();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                Toast.makeText(BasketActivity.this, R.string.connection_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void onSuccess() {

        dbContext.deleteAll();
        Toast.makeText(BasketActivity.this, R.string.add_order_successfully, Toast.LENGTH_SHORT).show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.basket_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.mnu_post_order:
                postOrder();
        }

        return super.onOptionsItemSelected(item);
    }


}
