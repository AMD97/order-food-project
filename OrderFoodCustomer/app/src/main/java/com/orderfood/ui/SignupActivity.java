package com.orderfood.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.orderfood.R;
import com.orderfood.model.SignupModel;
import com.orderfood.restAPI.AccountAPI;
import com.orderfood.restAPI.RestClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AMD on 5/23/2016.
 */

public class SignupActivity extends AppCompatActivity {

    public final static String USERNAME = "username";

    ProgressDialog progressDialog;
    EditText usernameText;
    EditText firstNameText;
    EditText lastNameText;
    EditText emailText;
    EditText passwordText;
    Button signupButton;
    TextView loginLink;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        usernameText = (EditText) findViewById(R.id.input_username);
        firstNameText = (EditText) findViewById(R.id.input_first_name);
        lastNameText = (EditText) findViewById(R.id.input_last_name);
        emailText = (EditText) findViewById(R.id.input_email);
        passwordText = (EditText) findViewById(R.id.input_password);
        signupButton = (Button) findViewById(R.id.btn_signup);
        loginLink = (TextView) findViewById(R.id.link_login);

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = new ProgressDialog(SignupActivity.this);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage(getString(R.string.create_account));
                progressDialog.show();
                signup();
            }
        });

        loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public void signup() {

        if (!validate()) {
            onSignupFailed(null);
            return;
        }

        signupButton.setEnabled(false);

        String username = usernameText.getText().toString();
        String firstName = firstNameText.getText().toString();
        String lastName = lastNameText.getText().toString();
        final String email = emailText.getText().toString();
        String password = passwordText.getText().toString();

        SignupModel signupModel = new SignupModel(email, username, firstName, lastName, password, password);

        AccountAPI api = RestClient.createService(AccountAPI.class, this);
        Call<SignupModel> call = api.signup(signupModel);

        call.enqueue(new Callback<SignupModel>() {
            @Override
            public void onResponse(Call<SignupModel> call, Response<SignupModel> response) {
                if (response.isSuccessful()) {
                    onSignupSuccess(response.body().getUsername());
                } else {
                    onSignupFailed(response.message());
                }
            }

            @Override
            public void onFailure(Call<SignupModel> call, Throwable throwable) {
                onSignupFailed(throwable.getMessage());
            }
        });
    }

    public void onSignupSuccess(String username) {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
        Intent intent = new Intent();
        intent.putExtra(USERNAME, username);
        setResult(RESULT_OK, intent);
        finish();
    }

    public void onSignupFailed(String error) {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
        if (error != null)
            Toast.makeText(SignupActivity.this, error, Toast.LENGTH_LONG).show();

        signupButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String username = usernameText.getText().toString();
        String firstName = firstNameText.getText().toString();
        String lastName = lastNameText.getText().toString();
        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();

        if (username.isEmpty() || username.length() < 5) {
            usernameText.setError(getString(R.string.unvalid_username));
            valid = false;
        } else {
            usernameText.setError(null);
        }

        if (firstName.isEmpty()) {
            firstNameText.setError(getString(R.string.unvalid_username));
            valid = false;
        } else {
            firstNameText.setError(null);
        }

        if (lastName.isEmpty()) {
            lastNameText.setError(getString(R.string.unvalid_username));
            valid = false;
        } else {
            lastNameText.setError(null);
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailText.setError(getString(R.string.unvalid_email));
            valid = false;
        } else {
            emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 6 || password.length() > 10) {
            passwordText.setError(getString(R.string.unvalid_password));
            valid = false;
        } else {
            passwordText.setError(null);
        }

        return valid;
    }
}