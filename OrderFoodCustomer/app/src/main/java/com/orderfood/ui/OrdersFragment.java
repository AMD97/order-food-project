package com.orderfood.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.orderfood.R;
import com.orderfood.model.OrderIndexModel;
import com.orderfood.restAPI.OrderAPI;
import com.orderfood.restAPI.RestClient;
import com.orderfood.ui.adapter.OrderIndexAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AMD on 5/21/2016.
 */
public class OrdersFragment extends Fragment {

    List<OrderIndexModel> orders;
    RecyclerView recyclerView;
    OrderIndexAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fraqment_orders, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.orderRecyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        getOrderAPI();
        return rootView;
    }

    private void getOrderAPI() {

        OrderAPI api = RestClient.createService(OrderAPI.class, getContext());
        Call<List<OrderIndexModel>> call = api.orderIndex();
        call.enqueue(new Callback<List<OrderIndexModel>>() {
            @Override
            public void onResponse(Call<List<OrderIndexModel>> call, Response<List<OrderIndexModel>> response) {
                if (response.isSuccessful()) {
                    orders = response.body();
                    onSuccess();
                }
            }

            @Override
            public void onFailure(Call<List<OrderIndexModel>> call, Throwable throwable) {
                Toast.makeText(MainActivity.mContext, R.string.connection_error, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void onSuccess() {

        adapter = new OrderIndexAdapter(this.orders);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

}
