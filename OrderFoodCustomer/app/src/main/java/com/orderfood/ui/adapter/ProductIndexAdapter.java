package com.orderfood.ui.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.orderfood.R;
import com.orderfood.model.ProductIndexModel;
import com.orderfood.ui.ProductDetailActivity;

import java.util.List;

/**
 * Created by AMD on 5/25/2016.
 */
public class ProductIndexAdapter extends RecyclerView.Adapter<ProductIndexAdapter.ViewHolder> {

    public final static String PRODUCT_ID = "productid";

    public AdapterView.OnItemClickListener mItemClickListener;

    public List<ProductIndexModel> products;

    public ProductIndexAdapter(List<ProductIndexModel> products) {
        this.products = products;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_product, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ProductIndexModel product = products.get(position);

        TextView nameTextView = holder.getNameTextView();
        nameTextView.setText(product.getName());

        TextView categoryTextView = holder.getCategoryTextView();
        categoryTextView.setText(product.getCategoryName());

        TextView priceTextView = holder.getPriceTextView();
        priceTextView.setText(String.valueOf(product.getPrice()));

    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView nameTextView;
        private TextView categoryTextView;
        private TextView priceTextView;
        private TextView shoeDetailTextView;

        public ViewHolder(final View itemView) {
            super(itemView);

            nameTextView = (TextView) itemView.findViewById(R.id.product_name);
            categoryTextView = (TextView) itemView.findViewById(R.id.pruduct_category);
            priceTextView = (TextView) itemView.findViewById(R.id.pruduct_price);
            shoeDetailTextView = (TextView) itemView.findViewById(R.id.pruduct_show_details);
            shoeDetailTextView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), ProductDetailActivity.class);
            int position = getLayoutPosition();
            int prodictID = products.get(position).getId();
            intent.putExtra(PRODUCT_ID, prodictID);
            v.getContext().startActivity(intent);
        }

        public TextView getNameTextView() {
            return nameTextView;
        }

        public TextView getCategoryTextView() {
            return categoryTextView;
        }

        public TextView getPriceTextView() {
            return priceTextView;
        }
    }
}
