package com.orderfood.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.orderfood.R;
import com.orderfood.dal.OrderDetailDB;
import com.orderfood.dal.entities.OrderDetail;
import com.orderfood.model.ProductModel;
import com.orderfood.restAPI.ProductAPI;
import com.orderfood.restAPI.RestClient;
import com.orderfood.ui.adapter.ProductIndexAdapter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AMD on 5/24/2016.
 */
public class ProductDetailActivity extends AppCompatActivity {

    CollapsingToolbarLayout collapsingToolbar;
    TextView descriptionTextView;
    TextView pricTextView;
    TextView categoryTextView;
    FloatingActionButton fab;

    ProductModel productModel;

    OrderDetailDB dbContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        collapsingToolbar.setTitle("");

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToBasket();
            }
        });

        descriptionTextView = (TextView) findViewById(R.id.description);
        pricTextView = (TextView) findViewById(R.id.price);
        categoryTextView = (TextView) findViewById(R.id.category);

        Intent intent = getIntent();
        int id = intent.getExtras().getInt(ProductIndexAdapter.PRODUCT_ID);
        getProductAPI(id);

    }

    private void getProductAPI(int id) {

        ProductAPI api = RestClient.createService(ProductAPI.class, this);
        Call<ProductModel> call = api.product(id);
        call.enqueue(new Callback<ProductModel>() {
            @Override
            public void onResponse(Call<ProductModel> call, Response<ProductModel> response) {
                if (response.isSuccessful())
                    productModel = response.body();
                onSuccess();
            }

            @Override
            public void onFailure(Call<ProductModel> call, Throwable throwable) {
                Toast.makeText(ProductDetailActivity.this, R.string.connection_error, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void onSuccess() {

        collapsingToolbar.setTitle(String.valueOf(productModel.getName()));
        descriptionTextView.setText(String.valueOf(productModel.getDescription()));
        pricTextView.setText(String.valueOf(productModel.getPrice()));
        categoryTextView.setText(String.valueOf(productModel.getCategoryName()));

    }

    private void addToBasket() {
        dbContext = new OrderDetailDB(this);
        OrderDetail detail = new OrderDetail(
                productModel.getId(),
                productModel.getName(),
                productModel.getPrice(),
                1
        );

        if (dbContext.exist(detail.getId())) {
            int quantity = dbContext.updateQuantity(detail.getId());
            String s = quantity + "اضافه شد. تعداد ";
            Toast.makeText(ProductDetailActivity.this, s, Toast.LENGTH_SHORT).show();
        } else {
            dbContext.create(detail);
            String s = "به سبد خرید اضافه شد";
            Toast.makeText(ProductDetailActivity.this, s, Toast.LENGTH_SHORT).show();
        }

    }
}
