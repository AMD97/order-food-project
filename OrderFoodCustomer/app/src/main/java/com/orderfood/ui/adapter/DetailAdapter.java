package com.orderfood.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.orderfood.R;
import com.orderfood.dal.entities.OrderDetail;
import com.orderfood.model.OrderDetailModel;

import java.util.List;

/**
 * Created by AMD on 5/31/2016.
 */
public class DetailAdapter  extends RecyclerView.Adapter<DetailAdapter.ViewHolder> {

        private List<OrderDetail> orderDetails;

        public DetailAdapter(List<OrderDetail> orderDetails) {
            this.orderDetails = orderDetails;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_order_detail, parent, false);

            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            OrderDetail orderDetail = orderDetails.get(position);

            TextView nameTextView = holder.getNameTextView();
            nameTextView.setText(orderDetail.getName());

            TextView priceTextView = holder.getPriceTextView();
            priceTextView.setText(String.valueOf(orderDetail.getPrice()));

            TextView quantityTextView = holder.getQuantityTextView();
            quantityTextView.setText(String.valueOf(orderDetail.getQuantity()));
        }

        @Override
        public int getItemCount() {
            return orderDetails.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private TextView nameTextView;
            private TextView priceTextView;
            private TextView quantityTextView;

            public ViewHolder(View itemView) {
                super(itemView);
                nameTextView = (TextView) itemView.findViewById(R.id.product_name);
                priceTextView = (TextView) itemView.findViewById(R.id.price);
                quantityTextView = (TextView) itemView.findViewById(R.id.quantity);
            }

            public TextView getPriceTextView() {
                return priceTextView;
            }

            public TextView getNameTextView() {
                return nameTextView;
            }

            public TextView getQuantityTextView() {
                return quantityTextView;
            }
        }

}
