package com.orderfood.ui;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.orderfood.R;
import com.orderfood.model.UpdateUserModel;
import com.orderfood.model.UserModel;
import com.orderfood.restAPI.AccountAPI;
import com.orderfood.restAPI.RestClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AMD on 5/29/2016.
 */
public class UserActivity extends AppCompatActivity {

    private EditText emailEditText;
    private EditText lastNameEditText;
    private EditText firstNameEditText;
    private EditText phoneNumberEditText;
    private EditText addressEditText;
    private EditText registrationDateEditText;
    private Button saveButton;
    private CollapsingToolbarLayout collapsingToolbar;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        collapsingToolbar.setTitle(" ");

        emailEditText = (EditText) findViewById(R.id.email);
        firstNameEditText = (EditText) findViewById(R.id.first_name);
        lastNameEditText = (EditText) findViewById(R.id.last_name);
        phoneNumberEditText = (EditText) findViewById(R.id.phone);
        addressEditText = (EditText) findViewById(R.id.address);
        registrationDateEditText = (EditText) findViewById(R.id.registrationDate);
        saveButton = (Button) findViewById(R.id.btn_save);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UpdateUserModel model = new UpdateUserModel(
                        firstNameEditText.getText().toString(),
                        lastNameEditText.getText().toString(),
                        phoneNumberEditText.getText().toString(),
                        addressEditText.getText().toString()
                );
                updateUserAPI(model);
            }
        });
        getUserAPI();
    }

    private void updateUserAPI(UpdateUserModel model) {

        AccountAPI api = RestClient.createService(AccountAPI.class, this);
        Call<Void> call = api.updateUser(model);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(UserActivity.this, R.string.update_successfully, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                onFailed(throwable.getMessage());
            }
        });

    }

    private void getUserAPI() {

        AccountAPI api = RestClient.createService(AccountAPI.class, this);
        Call<UserModel> call = api.getUser();
        call.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                if (response.isSuccessful()) {
                    onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable throwable) {
                onFailed(throwable.getMessage());
            }
        });

    }


    private void onSuccess(UserModel userModel) {

        collapsingToolbar.setTitle(userModel.getUsername());
        emailEditText.setText(userModel.getEmail());
        firstNameEditText.setText(userModel.getFirstName());
        lastNameEditText.setText(userModel.getLastName());
        phoneNumberEditText.setText(userModel.getPhoneNumber());
        addressEditText.setText(userModel.getAddress());
        registrationDateEditText.setText(userModel.getRegistrationDate());
    }

    private void onFailed(String error) {

        Toast.makeText(UserActivity.this, error, Toast.LENGTH_SHORT).show();

    }


}
