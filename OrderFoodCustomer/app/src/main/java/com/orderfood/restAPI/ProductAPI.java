package com.orderfood.restAPI;

import com.orderfood.model.ProductIndexModel;
import com.orderfood.model.ProductModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

/**
 * Created by AMD on 5/24/2016.
 */
public interface ProductAPI {

    @Headers({"Accept:application/json"
            ,"Content-Type:application/json"})
    @GET("api/Products/page/{page}")
    Call<List<ProductIndexModel>> productIndex(
            @Path("page") int page);

    @Headers({"Accept:application/json"
            ,"Content-Type:application/json"})
    @GET("api/Product/{id}")
    Call<ProductModel> product(
            @Path("id") int id);


}
