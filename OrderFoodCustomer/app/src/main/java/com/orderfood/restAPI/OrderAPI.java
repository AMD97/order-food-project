package com.orderfood.restAPI;

import com.orderfood.model.OrderCreateModel;
import com.orderfood.model.OrderDetailCreateModel;
import com.orderfood.model.OrderDetailModel;
import com.orderfood.model.OrderIndexModel;
import com.orderfood.model.OrderModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by AMD on 5/30/2016.
 */
public interface OrderAPI {

    @Headers({"Accept:application/json"
            , "Content-Type:application/json"})
    @GET("api/Orders/user")
    Call<List<OrderIndexModel>> orderIndex();

    @Headers({"Accept:application/json"
            , "Content-Type:application/json"})
    @GET("api/Order/{id}")
    Call<OrderModel> getOrder(
            @Path("id") int id);

    @Headers({"Accept:application/json"
            , "Content-Type:application/json"})
    @GET("api/OrderDetails/{id}")
    Call<List<OrderDetailModel>> getOrderDetail(
            @Path("id") int id);

    @Headers({"Accept:application/json"
            , "Content-Type:application/json"})
    @POST("api/Order")
    Call<Void> addOrder(
            @Body OrderCreateModel createModel);

}
