package com.orderfood.restAPI;

import com.orderfood.model.SignupModel;
import com.orderfood.model.TokenModel;
import com.orderfood.model.UpdateUserModel;
import com.orderfood.model.UserModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by AMD on 5/26/2016.
 */
public interface AccountAPI {

    @FormUrlEncoded
    @POST("oauth/token")
    Call<TokenModel> login(
            @Field("username") String username,
            @Field("password") String password,
            @Field("grant_type") String grantType);

    @Headers({"Accept:application/json"
            ,"Content-Type:application/json"})
    @POST("api/accounts/create")
    Call<SignupModel> signup(@Body SignupModel signupModel);

    @Headers({"Accept:application/json"
            ,"Content-Type:application/json"})
    @GET("api/accounts/user")
    Call<UserModel> getUser();

    @Headers({"Accept:application/json"
            ,"Content-Type:application/json"})
    @POST("api/accounts/update")
    Call<Void> updateUser(@Body UpdateUserModel updateUserModel);

}
