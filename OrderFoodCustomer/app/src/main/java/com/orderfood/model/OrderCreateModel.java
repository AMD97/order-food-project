package com.orderfood.model;

import java.util.List;

/**
 * Created by AMD on 5/31/2016.
 */
public class OrderCreateModel {

    private String firstName;
    private String lastName;
    private String phone;
    private String address;
    private boolean newInformation;
    private List<OrderDetailCreateModel> OrderDetails;

    public OrderCreateModel() {
    }

    public OrderCreateModel(String firstName, String lastName, String phone, String address, boolean newInformation, List<OrderDetailCreateModel> orderDetails) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.address = address;
        this.newInformation = newInformation;
        OrderDetails = orderDetails;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isNewInformation() {
        return newInformation;
    }

    public void setNewInformation(boolean newInformation) {
        this.newInformation = newInformation;
    }

    public List<OrderDetailCreateModel> getOrderDetails() {
        return OrderDetails;
    }

    public void setOrderDetails(List<OrderDetailCreateModel> orderDetails) {
        OrderDetails = orderDetails;
    }
}
