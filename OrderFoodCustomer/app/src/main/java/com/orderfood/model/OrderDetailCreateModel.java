package com.orderfood.model;

/**
 * Created by AMD on 5/31/2016.
 */
public class OrderDetailCreateModel {

    private int ID;
    private int Quantity;

    public OrderDetailCreateModel() {
    }

    public OrderDetailCreateModel(int ID, int quantity) {
        this.ID = ID;
        Quantity = quantity;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }
}
