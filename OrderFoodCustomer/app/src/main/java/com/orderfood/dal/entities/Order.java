package com.orderfood.dal.entities;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by AMD on 5/28/2016.
 */
@DatabaseTable
public class Order {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String Phone;

    @DatabaseField
    private String Address;

    @DatabaseField
    private double Total;

    public Order() {
    }

    public Order(int id, String phone, String address, double total ) {
        this.id = id;
        Phone = phone;
        Address = address;
        Total = total;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public double getTotal() {
        return Total;
    }

    public void setTotal(double total) {
        Total = total;
    }
}
