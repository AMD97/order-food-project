package com.orderfood.dal;

import android.content.Context;

import com.orderfood.dal.CRUD;
import com.orderfood.dal.entities.Order;
import com.orderfood.dal.entities.Order;
import android.util.Log;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by AMD on 5/31/2016.
 */
public class OrderDB implements CRUD<Order> {
    
    private DatabaseHelper dbContext;
    private final String TAG = "OrderDB";

    public OrderDB(Context context) {
        dbContext = new DatabaseHelper(context);
    }

    @Override
    public int create(Order object) {
        int index = -1;
        try {
            index = dbContext.OrderDao().create(object);
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        }
        return index;
    }

    @Override
    public int update(Order object) {
        int index = -1;
        try {
            dbContext.OrderDao().update(object);
        } catch (SQLException e) {
             Log.d(TAG, e.getMessage());
        }
        return index;
    }

    @Override
    public int delete(Order object) {
        int index = -1;
        try {
            dbContext.OrderDao().delete(object);
        } catch (SQLException e) {
             Log.d(TAG, e.getMessage());
        }
        return index;
    }

    @Override
    public Order findById(int id) {
        Order orderDetail = null;
        try {
            orderDetail = dbContext.OrderDao().queryForId(id);
        } catch (SQLException e) {
             Log.d(TAG, e.getMessage());
        }
        return orderDetail;
    }

    @Override
    public List<Order> findAll() {
        List<Order> items = null;
        try {
            items = dbContext.OrderDao().queryForAll();
        } catch (SQLException e) {
             Log.d(TAG, e.getMessage());
        }
        return items;
    }


}
