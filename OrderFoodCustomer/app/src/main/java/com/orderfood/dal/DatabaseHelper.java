package com.orderfood.dal;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.orderfood.dal.entities.Order;
import com.orderfood.dal.entities.OrderDetail;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by AMD on 5/28/2016.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "orderfood.sqlite";
    private static final int DATABASE_VERSION = 1;
    private final String TAG = "DatabaseHelper";

    private Dao<Order, Integer> OrderDao = null;
    private Dao<OrderDetail, Integer> OrderDetailDao = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Order.class);
            TableUtils.createTable(connectionSource, OrderDetail.class);
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, Order.class);
            TableUtils.dropTable(connectionSource, OrderDetail.class);
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        }
    }

    public Dao<Order, Integer> OrderDao() {

        if (null == OrderDao) {
            try {
                OrderDao = getDao(Order.class);
            } catch (java.sql.SQLException e) {
                Log.d(TAG, e.getMessage());
            }
        }
        return OrderDao;
    }

    public Dao<OrderDetail, Integer> OrderDetailDao() {

        if (null == OrderDetailDao) {
            try {
                OrderDetailDao = getDao(OrderDetail.class);
            } catch (java.sql.SQLException e) {
                Log.d(TAG, e.getMessage());
            }
        }
        return OrderDetailDao;
    }

}
