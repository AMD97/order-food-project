package com.orderfood.dal.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by AMD on 5/28/2016.
 */
@DatabaseTable
public class OrderDetail {

    @DatabaseField(id = true)
    private int id;

    @DatabaseField
    private String Name;

    @DatabaseField
    private double Price;

    @DatabaseField
    private int Quantity;

    public OrderDetail() {
    }

    public OrderDetail(int id, String name, double price, int quantity) {
        this.id = id;
        Name = name;
        Price = price;
        Quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }
}
