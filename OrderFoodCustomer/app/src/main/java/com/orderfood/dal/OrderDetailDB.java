package com.orderfood.dal;

import android.content.Context;

import com.orderfood.dal.entities.OrderDetail;
import android.util.Log;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by AMD on 5/31/2016.
 */
public class OrderDetailDB implements CRUD<OrderDetail> {

    private DatabaseHelper dbContext;
    private final String TAG = "OrderDetailDB";

    public OrderDetailDB(Context context) {
        dbContext = new DatabaseHelper(context);
    }

    @Override
    public int create(OrderDetail object) {
        int index = -1;
        try {
            index = dbContext.OrderDetailDao().create(object);
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        }
        return index;
    }

    @Override
    public int update(OrderDetail object) {
        int index = -1;
        try {
            dbContext.OrderDetailDao().update(object);
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        }
        return index;
    }

    @Override
    public int delete(OrderDetail object) {
        int index = -1;
        try {
            dbContext.OrderDetailDao().delete(object);
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        }
        return index;
    }

    @Override
    public OrderDetail findById(int id) {
        OrderDetail orderDetail = null;
        try {
            orderDetail = dbContext.OrderDetailDao().queryForId(id);
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        }
        return orderDetail;
    }

    @Override
    public List<OrderDetail> findAll() {
        List<OrderDetail> items = null;
        try {
            items = dbContext.OrderDetailDao().queryForAll();
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        }
        return items;
    }

    public boolean exist(int id) {
        boolean exist = false;
        try {
            exist = dbContext.OrderDetailDao().idExists(id);
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        }
        return exist;
    }

    public int updateQuantity(int id) {

        int quantity = 0;

        OrderDetail orderDetail = findById(id);
        quantity = orderDetail.getQuantity();
        orderDetail.setQuantity(++quantity);
        update(orderDetail);

        return quantity;
    }

    public int deleteAll() {
        int index = -1;
        try {
            dbContext.OrderDetailDao().delete(findAll());
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        }
        return index;
    }

}
