package com.orderfood.dal;

import java.util.List;

/**
 * Created by AMD on 5/29/2016.
 */
public interface CRUD<T> {

    public int create(T object);

    public int update(T object);

    public int delete(T object);

    public T findById(int id);

    public List<T> findAll();

}
