package com.admin.orderfood.restAPI;

import com.admin.orderfood.model.CategoryModel;
import com.admin.orderfood.model.ProductIndexModel;
import com.admin.orderfood.model.ProductModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by AMD on 5/24/2016.
 */
public interface ProductAPI {

    @Headers({"Accept:application/json"
            , "Content-Type:application/json"})
    @GET("api/Products/page/{page}")
    Call<List<ProductIndexModel>> productIndex(
            @Path("page") int page);

    @Headers({"Accept:application/json"
            , "Content-Type:application/json"})
    @GET("api/Product/{id}")
    Call<ProductModel> getProduct(
            @Path("id") int id);

    @Headers({"Accept:application/json"
            , "Content-Type:application/json"})
    @POST("api/Product/")
    Call<Void> addProduct(
            @Body ProductModel productModel);

    @Headers({"Accept:application/json"
            , "Content-Type:application/json"})
    @PUT("api/Product/{id}")
    Call<Void> updateProduct(
            @Path("id") int id,
            @Body ProductModel productModel);

    @Headers({"Accept:application/json"
            , "Content-Type:application/json"})
    @GET("api/Categories")
    Call<List<CategoryModel>> getCategories();

}
