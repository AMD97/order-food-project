package com.admin.orderfood.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.admin.orderfood.model.ProductModel;
import com.admin.orderfood.R;

import java.util.List;

/**
 * Created by AMD on 5/22/2016.
 */
public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    List<ProductModel> products;

    public ProductAdapter(List<ProductModel> products) {
        this.products = products;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_product, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ProductModel product = products.get(position);

        TextView nameTextView = holder.getNameTextView();
        nameTextView.setText(product.getName());

        TextView categoryTextView = holder.getCategoryTextView();
        categoryTextView.setText(product.getCategoryName());

        TextView priceTextView = holder.getPriceTextView();
        priceTextView.setText(String.valueOf(product.getPrice()));

    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView nameTextView;
        private TextView categoryTextView;
        private TextView priceTextView;
        private TextView shoeDetailTextView;

        public ViewHolder(final View itemView) {
            super(itemView);

            nameTextView = (TextView) itemView.findViewById(R.id.product_name);
            categoryTextView = (TextView) itemView.findViewById(R.id.pruduct_category);
            priceTextView = (TextView) itemView.findViewById(R.id.pruduct_price);
            shoeDetailTextView = (TextView) itemView.findViewById(R.id.pruduct_show_details);

        }

        public TextView getNameTextView() {
            return nameTextView;
        }

        public TextView getCategoryTextView() {
            return categoryTextView;
        }

        public TextView getPriceTextView() {
            return priceTextView;
        }
    }
}
