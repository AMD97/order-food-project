package com.admin.orderfood.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.admin.orderfood.R;
import com.admin.orderfood.model.OrderDetailModel;
import com.admin.orderfood.model.OrderModel;
import com.admin.orderfood.restAPI.OrderAPI;
import com.admin.orderfood.restAPI.RestClient;
import com.admin.orderfood.ui.adapter.OrderDetailAdapter;
import com.admin.orderfood.ui.adapter.OrderIndexAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AMD on 5/27/2016.
 */
public class OrderDetailActivity extends AppCompatActivity {

    OrderModel order;
    CollapsingToolbarLayout collapsingToolbar;
    TextView fullNameTextView;
    TextView phoneNumberTextView;
    TextView addressTextView;
    TextView dateTextView;
    TextView sumPriceTextView;

    List<OrderDetailModel> orderDetails;
    RecyclerView recyclerView;
    OrderDetailAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandedAppBarDisable);
        collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapsedAppBarDisable);
        collapsingToolbar.setTitle(null);

        recyclerView = (RecyclerView) findViewById(R.id.order_detail_RecyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        fullNameTextView = (TextView) findViewById(R.id.full_name);
        phoneNumberTextView = (TextView) findViewById(R.id.phone_number);
        addressTextView = (TextView) findViewById(R.id.address);
        dateTextView = (TextView) findViewById(R.id.date);
        sumPriceTextView = (TextView) findViewById(R.id.sum_price);

        Intent intent = getIntent();
        int id = intent.getExtras().getInt(OrderIndexAdapter.ORDER_ID);
        getOrderAPI(id);
        getOrderDetailAPI(id);
    }

    private void getOrderAPI(int id) {

        OrderAPI api = RestClient.createService(OrderAPI.class, this);
        Call<OrderModel> call = api.getOrder(id);
        call.enqueue(new Callback<OrderModel>() {
            @Override
            public void onResponse(Call<OrderModel> call, Response<OrderModel> response) {
                if (response.isSuccessful()) {
                    order = response.body();
                    onOrderSuccess();
                }
            }

            @Override
            public void onFailure(Call<OrderModel> call, Throwable throwable) {
                Toast.makeText(OrderDetailActivity.this, R.string.connection_error, Toast.LENGTH_LONG).show();
            }
        });
    }


    private void onOrderSuccess() {

        fullNameTextView.setText(order.getFirstName() + " " + order.getLastName());
        phoneNumberTextView.setText(order.getPhone());
        addressTextView.setText(order.getAddress());
        dateTextView.setText(order.getDate());
        sumPriceTextView.setText(String.valueOf(order.getTotal()));

    }

    private void getOrderDetailAPI(int id) {

        OrderAPI api = RestClient.createService(OrderAPI.class, this);
        Call<List<OrderDetailModel>> call = api.getOrderDetail(id);
        call.enqueue(new Callback<List<OrderDetailModel>>() {
            @Override
            public void onResponse(Call<List<OrderDetailModel>> call, Response<List<OrderDetailModel>> response) {
                if (response.isSuccessful()) {
                    orderDetails = response.body();
                    onOrderDetailSuccess();
                }
            }

            @Override
            public void onFailure(Call<List<OrderDetailModel>> call, Throwable throwable) {
                Toast.makeText(OrderDetailActivity.this, R.string.connection_error, Toast.LENGTH_LONG).show();
            }
        });
    }


    private void onOrderDetailSuccess() {

        adapter = new OrderDetailAdapter(orderDetails);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

}
