package com.admin.orderfood.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.admin.orderfood.R;
import com.admin.orderfood.model.OrderIndexModel;
import com.admin.orderfood.model.ProductIndexModel;
import com.admin.orderfood.restAPI.OrderAPI;
import com.admin.orderfood.restAPI.ProductAPI;
import com.admin.orderfood.restAPI.RestClient;
import com.admin.orderfood.ui.adapter.OrderIndexAdapter;
import com.admin.orderfood.ui.adapter.ProductIndexAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AMD on 5/21/2016.
 */
public class OrdersFragment extends Fragment {

    List<OrderIndexModel> orders;
    RecyclerView recyclerView;
    OrderIndexAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fraqment_orders, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.orderRecyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                getOrderAPI(page);
            }
        });

        getOrderAPI(1);
        return rootView;
    }

    private void getOrderAPI(int page) {

        OrderAPI api = RestClient.createService(OrderAPI.class, getContext());
        Call<List<OrderIndexModel>> call = api.orderIndex(page);
        call.enqueue(new Callback<List<OrderIndexModel>>() {
            @Override
            public void onResponse(Call<List<OrderIndexModel>> call, Response<List<OrderIndexModel>> response) {
                if (response.isSuccessful())
                    onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<OrderIndexModel>> call, Throwable throwable) {
                Toast.makeText(getContext(), R.string.connection_error, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void onSuccess(List<OrderIndexModel> orders) {

        if (this.orders == null) {
            this.orders = orders;
            adapter = new OrderIndexAdapter(this.orders);
            recyclerView.setAdapter(adapter);
        } else {
            int curSize = adapter.getItemCount();
            this.orders.addAll(orders);
            adapter.notifyItemRangeInserted(curSize, this.orders.size() - 1);
        }
    }

}
