package com.admin.orderfood.ui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.admin.orderfood.R;
import com.admin.orderfood.model.CategoryModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AMD on 5/30/2016.
 */
public class CategorySpinnerAdapter extends ArrayAdapter<CategoryModel> {

    public Resources res;
    LayoutInflater inflater;
    private Context context;
    private List<CategoryModel> data;

    public CategorySpinnerAdapter(Context context, int resource, List<CategoryModel> date) {
        super(context, resource, date);
        this.context = context;
        this.data = date;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        View row = inflater.inflate(R.layout.item_category_spinner, parent, false);

        TextView item = (TextView) row.findViewById(R.id.item);

        item.setText(data.get(position).getName());

        return row;
    }

}
