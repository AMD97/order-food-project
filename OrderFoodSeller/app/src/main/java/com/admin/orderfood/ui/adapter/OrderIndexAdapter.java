package com.admin.orderfood.ui.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.admin.orderfood.R;
import com.admin.orderfood.model.OrderIndexModel;
import com.admin.orderfood.ui.OrderDetailActivity;
import com.admin.orderfood.ui.ProductDetailActivity;

import java.util.List;

/**
 * Created by AMD on 5/21/2016.
 */
public class OrderIndexAdapter extends RecyclerView.Adapter<OrderIndexAdapter.ViewHolder> {

    public final static String ORDER_ID = "orderid";
    private List<OrderIndexModel> orders;

    public OrderIndexAdapter(List<OrderIndexModel> orders) {
        this.orders = orders;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_order, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        OrderIndexModel order = orders.get(position);

        TextView totalTextView = holder.getTotalTextView();
        totalTextView.setText(String.valueOf(order.getTotal()));

        TextView dateTextView = holder.getDateTextView();
        dateTextView.setText(order.getDate());
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView dateTextView;
        private TextView totalTextView;
        private TextView showDetailTextView;

        public ViewHolder(View view) {
            super(view);
            dateTextView = (TextView) view.findViewById(R.id.order_Date);
            totalTextView = (TextView) view.findViewById(R.id.order_total);
            showDetailTextView = (TextView) view.findViewById(R.id.order_show_details);
            showDetailTextView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), OrderDetailActivity.class);
            int position = getLayoutPosition();
            int orderID = orders.get(position).getId();
            intent.putExtra(ORDER_ID, orderID);
            v.getContext().startActivity(intent);
        }

        public TextView getDateTextView() {
            return dateTextView;
        }

        public TextView getTotalTextView() {
            return totalTextView;
        }
    }
}
