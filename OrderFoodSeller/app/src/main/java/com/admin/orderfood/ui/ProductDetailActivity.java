package com.admin.orderfood.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.admin.orderfood.R;
import com.admin.orderfood.model.CategoryModel;
import com.admin.orderfood.model.ProductModel;
import com.admin.orderfood.restAPI.ProductAPI;
import com.admin.orderfood.restAPI.RestClient;
import com.admin.orderfood.ui.adapter.CategorySpinnerAdapter;
import com.admin.orderfood.ui.adapter.ProductIndexAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AMD on 5/24/2016.
 */
public class ProductDetailActivity extends AppCompatActivity {

    ProductModel productModel;
    List<CategoryModel> categoryModel;

    TextView nameTextView;
    TextView descriptionTextView;
    TextView priceTextView;
    Spinner categorySpinner;
    Button saveButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.product_details);
        setSupportActionBar(toolbar);


        nameTextView = (TextView) findViewById(R.id.name);
        descriptionTextView = (TextView) findViewById(R.id.description);
        priceTextView = (TextView) findViewById(R.id.price);
        categorySpinner = (Spinner) findViewById(R.id.categoty_spinner);

        saveButton = (Button) findViewById(R.id.btn_save);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProductAPI();
            }
        });
        Intent intent = getIntent();
        int id = intent.getExtras().getInt(ProductIndexAdapter.PRODUCT_ID);
        getProductAPI(id);
        getCategory();
    }

    private void getCategory() {

        ProductAPI api = RestClient.createService(ProductAPI.class, this);
        Call<List<CategoryModel>> call = api.getCategories();
        call.enqueue(new Callback<List<CategoryModel>>() {
            @Override
            public void onResponse(Call<List<CategoryModel>> call, Response<List<CategoryModel>> response) {
                if (response.isSuccessful()) {
                    categoryModel = response.body();
                    showCategoryData();
                }
            }

            @Override
            public void onFailure(Call<List<CategoryModel>> call, Throwable throwable) {
                Toast.makeText(ProductDetailActivity.this, R.string.connection_error, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void showCategoryData() {
        CategorySpinnerAdapter adapter = new CategorySpinnerAdapter(this, R.id.item, categoryModel);
        categorySpinner.setAdapter(adapter);

    }

    private void getProductAPI(int id) {

        ProductAPI api = RestClient.createService(ProductAPI.class, this);
        Call<ProductModel> call = api.getProduct(id);
        call.enqueue(new Callback<ProductModel>() {
            @Override
            public void onResponse(Call<ProductModel> call, Response<ProductModel> response) {
                if (response.isSuccessful()) {
                    productModel = response.body();
                    showData();
                }
            }

            @Override
            public void onFailure(Call<ProductModel> call, Throwable throwable) {
                Toast.makeText(ProductDetailActivity.this, R.string.connection_error, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void showData() {
        nameTextView.setText(String.valueOf(productModel.getName()));
        descriptionTextView.setText(String.valueOf(productModel.getDescription()));
        priceTextView.setText(String.valueOf(productModel.getPrice()));
    }

    private void updateProductAPI() {
        ProductModel model = getData();
        ProductAPI api = RestClient.createService(ProductAPI.class, this);
        Call<Void> call = api.updateProduct(model.getId(), model);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful())
                    Toast.makeText(ProductDetailActivity.this, R.string.update_successfully, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                Toast.makeText(ProductDetailActivity.this, R.string.connection_error, Toast.LENGTH_LONG).show();
            }
        });
    }

    private ProductModel getData() {
        CategoryModel categoryModel = (CategoryModel) categorySpinner.getItemAtPosition(categorySpinner.getSelectedItemPosition());

        return  new ProductModel(
                this.productModel.getId(),
                nameTextView.getText().toString(),
                descriptionTextView.getText().toString(),
                Integer.valueOf(priceTextView.getText().toString()),
                categoryModel.getId(),
                null);
    }
}
