package com.admin.orderfood.restAPI;

import com.admin.orderfood.model.OrderDetailModel;
import com.admin.orderfood.model.OrderIndexModel;
import com.admin.orderfood.model.OrderModel;
import com.admin.orderfood.model.ProductIndexModel;
import com.admin.orderfood.model.ProductModel;
import com.admin.orderfood.model.SignupModel;
import com.admin.orderfood.model.UserModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by AMD on 5/30/2016.
 */
public interface OrderAPI {

    @Headers({"Accept:application/json"
            , "Content-Type:application/json"})
    @GET("api/Orders/page/{page}")
    Call<List<OrderIndexModel>> orderIndex(
            @Path("page") int page);

    @Headers({"Accept:application/json"
            , "Content-Type:application/json"})
    @GET("api/Order/{id}")
    Call<OrderModel> getOrder(
            @Path("id") int id);

    @Headers({"Accept:application/json"
            , "Content-Type:application/json"})
    @GET("api/OrderDetails/{id}")
    Call<List<OrderDetailModel>> getOrderDetail(
            @Path("id") int id);



}
