package com.admin.orderfood.restAPI;

import android.content.Context;

import com.admin.orderfood.model.ErrorModel;
import com.admin.orderfood.ui.LoginActivity;
import com.admin.orderfood.ui.MainActivity;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by AMD on 5/24/2016.
 */
public class RestClient {

    public static final String API_BASE_URL = "http://192.168.43.217:34249/";

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    public static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build());

    public static <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = builder.build();
        return retrofit.create(serviceClass);
    }

    public static <S> S createService(Class<S> serviceClass, Context context) {

        final String authToken = context.getSharedPreferences(MainActivity.PREFS_NAME, context.MODE_PRIVATE).getString(LoginActivity.PREFS_TOKEN, null);

        if (authToken != null) {
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Authorization", "bearer " + authToken)
                            .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });
        }

        OkHttpClient client = httpClient.build();
        Retrofit retrofit = builder.client(client).build();
        return retrofit.create(serviceClass);
    }

    public static <S> ErrorModel ErrorHandler(Response<S> response) {
        Converter<ResponseBody, ErrorModel> converter =
                builder.build().responseBodyConverter(ErrorModel.class, new Annotation[0]);
        ErrorModel errorModel = new ErrorModel();
        try {
            errorModel = converter.convert(response.errorBody());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return errorModel;
    }

}
