package com.admin.orderfood.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.admin.orderfood.R;
import com.admin.orderfood.model.ProductIndexModel;
import com.admin.orderfood.restAPI.ProductAPI;
import com.admin.orderfood.restAPI.RestClient;
import com.admin.orderfood.ui.adapter.ProductIndexAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AMD on 5/20/2016.
 */
public class ProductsFragment extends Fragment {

    List<ProductIndexModel> products;
    RecyclerView recyclerView;
    ProductIndexAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_products, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.productRecyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                getProductAPI(page);
            }
        });

        getProductAPI(1);
        return rootView;
    }

    private void getProductAPI(int page) {

        ProductAPI api = RestClient.createService(ProductAPI.class, getContext());
        Call<List<ProductIndexModel>> call = api.productIndex(page);
        call.enqueue(new Callback<List<ProductIndexModel>>() {
            @Override
            public void onResponse(Call<List<ProductIndexModel>> call, Response<List<ProductIndexModel>> response) {
                if (response.isSuccessful())
                    onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<ProductIndexModel>> call, Throwable throwable) {
                Toast.makeText(getContext(), R.string.connection_error, Toast.LENGTH_LONG).show();

            }
        });
    }

    private void onSuccess(List<ProductIndexModel> products) {

        if (this.products == null) {
            this.products = products;
            adapter = new ProductIndexAdapter(this.products);
            recyclerView.setAdapter(adapter);
        } else {
            int curSize = adapter.getItemCount();
            this.products.addAll(products);
            adapter.notifyItemRangeInserted(curSize, this.products.size() - 1);
        }
    }

    private void onFailed() {

    }
}
