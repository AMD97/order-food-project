package com.admin.orderfood.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.admin.orderfood.R;
import com.admin.orderfood.model.ErrorModel;
import com.admin.orderfood.model.TokenModel;
import com.admin.orderfood.restAPI.AccountAPI;
import com.admin.orderfood.restAPI.RestClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AMD on 5/22/2016.
 */
public class LoginActivity extends AppCompatActivity {
    public final static String PREFS_LOGIN = "login";
    public final static String PREFS_TOKEN = "token";
    private static final int REQUEST_SIGNUP = 1;
    EditText usernameTextView;
    EditText passwordTextView;
    Button loginButton;
    TextView signupLink;
    ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usernameTextView = (EditText) findViewById(R.id.input_username);
        passwordTextView = (EditText) findViewById(R.id.input_password);
        loginButton = (Button) findViewById(R.id.btn_login);
        signupLink = (TextView) findViewById(R.id.link_signup);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = new ProgressDialog(LoginActivity.this);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage(getString(R.string.Authenticating));
                progressDialog.show();
                login();
            }
        });

        signupLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
            }
        });
    }

    public void login() {

        if (!validate()) {
            onLoginFailed(null);
        }

        loginButton.setEnabled(false);


        String username = usernameTextView.getText().toString();
        String password = passwordTextView.getText().toString();

        AccountAPI api = RestClient.createService(AccountAPI.class,this);
        Call<TokenModel> call = api.login(username, password, "password");
        call.enqueue(new Callback<TokenModel>() {
            @Override
            public void onResponse(Call<TokenModel> call, Response<TokenModel> response) {
                if (response.isSuccessful()) {
                    onLoginSuccess(response.body());
                } else {
                    ErrorModel errorModel = RestClient.ErrorHandler(response);
                    onLoginFailed(errorModel.toString());
                }
            }

            @Override
            public void onFailure(Call<TokenModel> call, Throwable throwable) {
                onLoginFailed(throwable.getMessage());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(LoginActivity.this, "جساب کاربری ایجاد شد. کلمه عبور خود را وارد نمایید.", Toast.LENGTH_SHORT).show();
                usernameTextView.setText(data.getExtras().getString(SignupActivity.USERNAME));
                passwordTextView.setText(null);
            }
        }
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    public void onLoginSuccess(TokenModel tokenModel) {
        progressDialog.setMessage("اعتبار سنجی موفق بود. در حال آماده سازی...");

        SharedPreferences preferences = getSharedPreferences(MainActivity.PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(PREFS_LOGIN, true);
        editor.putString(PREFS_TOKEN, tokenModel.getAccessToken());
        editor.commit();

        if (progressDialog.isShowing())
            progressDialog.dismiss();

        finish();
    }

    public void onLoginFailed(String error) {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
        if (error != null)
            Toast.makeText(LoginActivity.this, error, Toast.LENGTH_LONG).show();

        loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String username = usernameTextView.getText().toString();
        String password = passwordTextView.getText().toString();

        if (username.isEmpty() || username.length() < 5 || username.length() > 10) {
            usernameTextView.setError(getString(R.string.unvalid_username));
            valid = false;
        } else {
            usernameTextView.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            passwordTextView.setError(getString(R.string.unvalid_password));
            valid = false;
        } else {
            passwordTextView.setError(null);
        }

        return valid;
    }
}
