package com.admin.orderfood.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.admin.orderfood.R;
import com.admin.orderfood.model.CategoryModel;
import com.admin.orderfood.model.ProductModel;
import com.admin.orderfood.restAPI.ProductAPI;
import com.admin.orderfood.restAPI.RestClient;
import com.admin.orderfood.ui.adapter.CategorySpinnerAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AMD on 5/30/2016.
 */
public class AddProductActivity extends AppCompatActivity {

    ProductModel productModel;
    List<CategoryModel> categoryModel;

    TextView nameTextView;
    TextView descriptionTextView;
    TextView priceTextView;
    Spinner categorySpinner;
    Button saveButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.add_product);
        setSupportActionBar(toolbar);

        nameTextView = (TextView) findViewById(R.id.name);
        descriptionTextView = (TextView) findViewById(R.id.description);
        priceTextView = (TextView) findViewById(R.id.price);
        categorySpinner = (Spinner) findViewById(R.id.categoty_spinner);

        saveButton = (Button) findViewById(R.id.btn_save);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addProductAPI();
            }
        });
        getCategory();
    }

    private void getCategory() {

        ProductAPI api = RestClient.createService(ProductAPI.class, this);
        Call<List<CategoryModel>> call = api.getCategories();
        call.enqueue(new Callback<List<CategoryModel>>() {
            @Override
            public void onResponse(Call<List<CategoryModel>> call, Response<List<CategoryModel>> response) {
                if (response.isSuccessful()) {
                    categoryModel = response.body();
                    showCategoryData();
                }
            }

            @Override
            public void onFailure(Call<List<CategoryModel>> call, Throwable throwable) {
                Toast.makeText(AddProductActivity.this, R.string.connection_error, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void showCategoryData() {
        CategorySpinnerAdapter adapter = new CategorySpinnerAdapter(this, R.id.item, categoryModel);
        categorySpinner.setAdapter(adapter);
    }

    private void addProductAPI() {
        ProductModel model = setData();
        ProductAPI api = RestClient.createService(ProductAPI.class, this);
        Call<Void> call = api.addProduct(model);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful())
                    Toast.makeText(AddProductActivity.this, R.string.add_product_successfully, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                Toast.makeText(AddProductActivity.this, R.string.connection_error, Toast.LENGTH_LONG).show();
            }
        });
    }

    private ProductModel setData() {
        CategoryModel categoryModel = (CategoryModel) categorySpinner.getItemAtPosition(categorySpinner.getSelectedItemPosition());

        return new ProductModel(
                0,
                nameTextView.getText().toString(),
                descriptionTextView.getText().toString(),
                Integer.valueOf(priceTextView.getText().toString()),
                categoryModel.getId(),
                null);
    }


}
